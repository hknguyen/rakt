#!/usr/bin/env python

import sys
import os
import argparse
import textwrap
from ConfigParser import SafeConfigParser

from util.dboutput import db_query
from util.dboutput import db_read
from sources.blist import BlistData
from sources.jigsaw import JigData
from sources.linkedin import LinkinLog

VERSION = '0.2'

# Instantiates the config parser object
def initConfig():
    configDict = {}  
    parser = SafeConfigParser()
    parser.read('config.ini')

    configDict['cx_key'] = parser.get('google_api', 'cx_key')
    configDict['api_key'] = parser.get('google_api', 'api_key')
    configDict['consumer_key'] = parser.get('oauth_linkedin', 'consumer_key')
    configDict['consumer_secret'] = parser.get('oauth_linkedin', 'consumer_secret')
    configDict['user_token'] = parser.get('oauth_linkedin', 'user_token')
    configDict['user_secret'] = parser.get('oauth_linkedin', 'user_secret')
    configDict['j_email'] = parser.get('jigsaw_email', 'j_email')

    if configDict['cx_key'] == "" or configDict['api_key'] == "":
        print "[-] No Google API or Key set in config.ini, exiting..."
        sys.exit(0)
    else:
        return configDict

def main():
    DBFLAG = False
    
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
            description=textwrap.dedent('''\
                Info: RAKT was created by Hao Nguyen and Chris Patten
                Purpose: Scrape contact information from business-lists.com, list-brokers.com, connect.data.com and linkedin.com
                Contact: neygun[t.a.]gmail.com; @neygun
                Contact: cpatten[t.a.]packetresearch.com; @packetassailant
               '''))
    subparsers = parser.add_subparsers()
    scrape_parser = subparsers.add_parser('scrape')
    outputs_group = scrape_parser.add_mutually_exclusive_group(required=True)
    outputs_group.add_argument('-o', '--outfile', help="Output text file")
    outputs_group.add_argument('-d', '--dboutfile', help="Output LevelDB file")
    inputs_group = scrape_parser.add_mutually_exclusive_group(required=True)
    inputs_group.add_argument('-s', '--search', help="Company to search for in quotes (e.g. 'Company Name')")
    inputs_group.add_argument('-i', '--infile', help="List of business-lists URLs")
    sources_group = scrape_parser.add_argument_group(description="Select source to scrape, no flags default to all sources")
    sources_group.add_argument('-b', '--blists', help="Scrape business-lists", action="append_const", dest="source_items", const="blists")
    sources_group.add_argument('-j', '--jigsaw', help="Scrape connect.data.com, PhantomJS required, make sure to install it before running the tool", action="append_const", dest="source_items", const="jigsaw")
    sources_group.add_argument('-l', '--linkedin', help="Scrape linkedin", action="append_const", dest="source_items", const="linkedin")
    scrape_parser.add_argument('-p', '--proxy', help="Proxy Host:Port (e.g. 127.0.0.1:8080)", required=False)

    query_parser = subparsers.add_parser('query')
    search_group = query_parser.add_argument_group(description="Select employee record item to query")
    search_group.add_argument('-a', '--all', help="All", action="append_const", dest="search_items", const="All")
    search_group.add_argument('-n', '--name', help="Name", action="append_const", dest="search_items", const="Employee")
    search_group.add_argument('-t', '--title', help="Title", action="append_const", dest="search_items", const="Role")
    search_group.add_argument('-e', '--email', help="Email", action="append_const", dest="search_items", const="Email Address")
    search_group.add_argument('-p', '--phone', help="Phone", action="append_const", dest="search_items", const="Generic Phone")
    search_group.add_argument('-l', '--location', help="Location", action="append_const", dest="search_items", const="City")
    query_parser.add_argument('-d', '--dbfile', type=str, help="Database to search", required=True)

    parser.add_argument('-v', '--version', action='version', version="Current Version: "+VERSION)
    args = parser.parse_args()
    
    configDict = initConfig()
    try:
        if args.search_items and args.dbfile:
            search_items = args.search_items
            if 'Role' in search_items:
                title_location = search_items.index('Role')
            if 'Generic Phone' in search_items:
                phone_location = search_items.index('Generic Phone')
                search_items.insert(phone_location+1, 'Direct Phone')
            if 'City' in search_items:
                city_location = search_items.index('City')
                search_items.insert(city_location+1, 'State')
            if 'All' in search_items:
                search_items = ['Employee', 'Role', 'Email Address', 'Generic Phone', 'Direct Phone', 'City', 'State', 'Source']
            db_query(args.dbfile, search_items)
        else:
            print "[-] Query terms required..."
            sys.exit(2)
    except AttributeError:
        if args.dboutfile:
            DBFLAG = True
            output = args.dboutfile
        elif args.outfile:
            output = args.outfile
        if args.source_items:
            source_items = args.source_items
            if 'blists' in source_items:
                #Only scrape blists
                print "[+] Scraping business-lists..."
                if args.search:
                    name = args.search
                    blist_scrape = BlistData(name, output, args.proxy, DBFLAG)
                    blist_data = blist_scrape.get_master_request(configDict)
                elif args.infile:
                    infile = open(args.infile, 'r')
                    blist_scrape = BlistData(None, output, args.proxy, DBFLAG)
                    blist_data = blist_scrape.get_multi_url_request(infile)
            if 'jigsaw' in source_items:
                #Only scrape jigsaw
                print "[+] Scraping data.com..."
                if args.infile:
                    if 'blists' not in source_items:
                        print "[-] URL file is for scraping business-lists only"
                    name = raw_input("Enter the company to search for connect.data.com page: ")
                else:
                    name = args.search
                jig_scrape = JigData(name, output, configDict, args.proxy, DBFLAG)
                
                if 'blists' in source_items:
                    jig_scrape.email_format(blist_data)
                elif args.dboutfile:
                    print "[+] Database provided, reading records to determine email address format..."
                    db_data = db_read(args.dboutfile)
                    jig_scrape.email_format(db_data)
                elif args.outfile:
                    jig_scrape.email_format([])
                jig_scrape.get_requests(configDict)
            if 'linkedin' in source_items:
                #Only scrape linkedin
                if args.search:
                    name = args.search
                    print "[+] Scraping linkedin..."
                    linkin_scrape = LinkinLog(name, output, configDict, jig_scrape.domain, args.proxy, DBFLAG)
                    peopleList = linkin_scrape.oauthSetter()
                    linkin_scrape.formatResults(peopleList, jig_scrape.emailformat)
                else:
                    print "[-] URL file is for scraping business-lists only"
        elif not args.source_items:
            print "[+] No data sources specifically selected, defaulting to all..."
            if args.search:
                name = args.search
                blist_scrape = BlistData(name, output, args.proxy, DBFLAG)
                blist_data = blist_scrape.get_master_request(configDict)
            elif args.infile:
                infile = open(args.infile, 'r')
                blist_scrape = BlistData(None, output, args.proxy, DBFLAG)
                blist_data = blist_scrape.get_multi_url_request(infile)
                name = raw_input("Enter the company to search for connect.data.com page: ")
            jig_scrape = JigData(name, output, args.proxy, DBFLAG)
            jig_scrape.email_format(blist_data)
            jig_scrape.get_requests(configDict)
            linkin_scrape = LinkinLog(name, output, configDict, jig_scrape.domain, args.proxy, DBFLAG)
            peopleList = linkin_scrape.oauthSetter()
            linkin_scrape.formatResults(peopleList, jig_scrape.emailformat)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print "[-] Caught KeyboardInterrupt, exiting..."
        sys.exit(0)
