#!/usr/bin/env python

import sys
import os
import plyvel
import json

def db_query(database, search_items):
    DELIMITER = ';'
    if len(search_items) == 1:
        DELIMITER=''
    db = plyvel.DB(database)
    for item in search_items:
        sys.stdout.write(item+DELIMITER)
    print "\n",
    for key, value in db:
        for item in search_items:
            try:
                sys.stdout.write(json.loads(value)[item]+DELIMITER)
            except:
                sys.stdout.write("Not Found"+DELIMITER)
        print "\n",

def db_read(database):
    search_items = ['Employee', 'Role', 'Email Address', 'Generic Phone', 'Direct Phone', 'City', 'State', 'Source']
    db_data = []
    try:
        db = plyvel.DB(database)
        for key, value in db:
            emp = {}
            for item in search_items:
                try:
                    emp[item] = json.loads(value)[item]
                except:
                    emp[item] = "Not Found"
            db_data.append(emp)
    except:
        print "[-] Empty database provided, select an email address format to continue:"
    return db_data
