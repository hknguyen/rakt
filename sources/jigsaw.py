#!/usr/bin/env python

import sys
import grequests 
import requests
import re
from bs4 import BeautifulSoup
import getpass
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from prettytable import PrettyTable
from progressbar import ProgressBar
import math
from util.googlesearch import gSearch
from util.output import write_outfile
from util.output import write_dbfile

class JigData:
    def __init__(self, name, outfile, configDict, proxy, dbflag):
        self.comp_name = name
        self.glist = [] 
        self.outfile = outfile
        self.j_email = configDict['j_email']
        self.emailformat = ""
        self.domain = ""
        self.jig_cookie =  ""
        self.proxies = {}
        self.proxy = ""
        if proxy != None:
            self.proxy = proxy
            self.proxies['http'] = 'http://' + proxy
            self.proxies['https'] = 'https://' + proxy
        self.headers = { \
               'Host': 'connect.data.com', \
               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0', \
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', \
               'Accept-Language': 'en-US,en;q=0.5', \
               'Accept-Encoding': 'gzip, deflate', \
               'Connection': 'keep-alive'
               }
        self.dbflag = dbflag

    def email_format(self, emp_list):
        format_id = 0
        format_counts = []
        choice = -1
        ready = False
        domains = []
        domains_dict = {}
        formats = {"<first initial><last name>": 0,
                "<first initial>.<last name>": 0,
                "<first name><last name>": 0,
                "<first name>.<last name>": 0,
                "<first name>": 0,
                "<last name>": 0,
                "<first name><last initial>": 0,
                "<first name>.<last initial>": 0,
                "<last name><first initial>": 0,
                "<last name>.<first initial>": 0,
                "<last name><first name>": 0,
                "<last name>.<first name>": 0,
                "<first name>.<middle initial>.<last name>": 0,
                "other": 0}
        results_dict = {}
        results = []
        theader = PrettyTable([
            "ID",
            "Email Format",
            "Count"
            ])
        theader.align["ID"] = "l"
        theader.align["Email Format"] = "l"
        theader.align["Count"] = "l"
        theader.padding_width = 1

        theader_domain = PrettyTable([
            "ID",
            "Domain",
            "Count"
            ])
        theader_domain.align["ID"] = "l"
        theader_domain.align["Domain"] = "l"
        theader_domain.align["Count"] = "l"
        theader_domain.padding_width = 1

        for emp in emp_list:
            name = emp['Employee'].lower()
            space_count = name.count(' ')
            if space_count == 1:
                fname = name[0:name.find(' ')]
                lname = name[name.find(' ')+1:len(name)]
            elif space_count > 1:
                fname = name[0:name.find(' ')]
                lname = name[name.rfind(' ')+1:len(name)]
            
            email = emp['Email Address'].lower()
            email_name = email[0:email.find('@')]
            period_count = email_name.count('.')
            domain = email[email.find('@'):len(email)]
            domains.append(domain)

            period_pos = email_name.find('.')
            fname_pos = email_name.find(fname)
            lname_pos = email_name.find(lname)

            if period_pos == -1 and lname_pos == 1 and (email_name.find(fname[0]) == 0) and fname_pos == -1: 
                formats['<first initial><last name>'] += 1
            elif period_pos == 1 and lname_pos == 2 and email_name.find(fname[0]) == 0 and fname_pos == -1:
                formats['<first initial>.<last name>'] += 1
            elif period_pos == -1 and lname_pos > 1 and fname_pos == 0 and (len(fname) + len(lname)) == len(email_name): 
                formats['<first name><last name>'] += 1
            elif period_pos == (lname_pos - 1) and lname_pos > 1 and fname_pos == 0: 
                formats['<first name>.<last name>'] += 1
            elif fname == email_name:
                formats['<first name>'] += 1
            elif lname == email_name:
                formats['<last name>'] += 1
            elif period_pos == -1 and email_name.rfind(lname[0]) == (len(email_name) - 1) and fname_pos == 0 and lname_pos == -1:
                formats['<first name><last initial>'] += 1
            elif period_pos == len(fname) and email_name.rfind(lname[0]) == (len(email_name) - 1) and fname_pos == 0 and lname_pos == -1:
                formats['<first name>.<last initial>'] += 1
            elif period_pos == -1 and lname_pos == 0 and email_name.rfind(fname[0]) == (len(email_name) - 1) and fname_pos == -1:
                formats['<last name><first initial>'] += 1
            elif period_pos == len(lname) and lname_pos == 0 and email_name.rfind(fname[0]) == (len(email_name) - 1) and fname_pos == -1:
                formats['<last name>.<first initial>'] += 1
            elif period_pos == -1 and fname_pos > 1 and lname_pos == 0 and (len(fname) + len(lname)) == len(email_name): 
                formats['<last name><first name>'] += 1
            elif period_pos == (fname_pos - 1) and fname_pos > 1 and lname_pos == 0: 
                formats['<last name>.<first name>'] += 1
            elif period_count == 2 and fname_pos == 0 and lname_pos == (email_name.rfind('.') + 1) and (email_name.rfind('.') == email.find('.') + 2):
                formats['<first name>.<middle initial>.<last name>'] += 1
            else:
                formats['other'] += 1
                print "[-] Unknown Format: " + emp['Employee'] + " - " + emp['Email Address']

        for key, value in formats.items():
            results_dict['format_id'] = format_id
            results_dict['format'] = key
            results_dict['num'] = value
            results.append(results_dict.copy())
            
            theader.add_row([
                "[" + str(format_id) + "]",
                key,
                value
                ])
            format_counts.append(value)
            format_id += 1
        
        print "\n[+] Determining email format..."
        print theader
        
        for c in reversed(sorted(format_counts)):
            pick = [ result['format_id'] for result in results if result['num'] is c ]
            invalid_format = [ r['format'] for r in results if r['format_id'] is pick[0] ]
            if invalid_format[0] != "other" and invalid_format[0] != "<first name>.<middle initial>.<last name>":
                valid_pick_count = c
                break
        default_choice = [ result['format_id'] for result in results if result['num'] is valid_pick_count ]

        while not ready:
            try:
                choice = int(raw_input("Select the format to use for generating email addresses: "))
                if choice > len(formats) or int(choice) < 0:
                    print "[-] Invalid choice, choice has to be 0 - " + str(len(results_dict))
                    print "[+] Highest count for a valid format is " + str(valid_pick_count)
                    choice = default_choice[0]
                    print "[+] Defaulting to choice %s" % choice
                    ready = True
                else:
                    emailformat = [ r['format'] for r in results if r['format_id'] is choice ]
                    if emailformat[0] == "other" or emailformat[0] == "<first name>.<middle initial>.<last name>":
                        print "[-] Invalid choice, can't generate this email address format from connect.data.com records"
                        print "[+] Highest count for a valid format is " + str(valid_pick_count)
                        choice = default_choice[0]
                        print "[+] Defaulting to choice %s" % choice
                        ready = True
                    else:
                        ready = True
            except:
                print "[-] Invalid choice, not a number"
                print "[+] Highest count for a valid format is " + str(valid_pick_count)
                choice = default_choice[0]
                print "[+] Defaulting to choice %s" % choice
                ready = True

        self.emailformat = [ r['format'] for r in results if r['format_id'] is choice ]
        print "[+] Generating email addresses using the '" + self.emailformat[0] + "' format"
        
        if len(emp_list) == 0:
            self.domain = raw_input("[+] Enter the domain for generating email addresses (e.g @company.com): ")
        else:
            if len(set(domains)) == 1:
                self.domain = domain
                print "[+] Only one domain found, using %s for generating email addresses" % (self.domain)
            else:
                print "\n[+] Discovered domains:"
                temp_domains = domains
                for i, d in enumerate(set(domains)):
                    domains_dict[i] = d
                    domains_dict['count'] = temp_domains.count(d)
                    theader_domain.add_row([
                        "[" + str(i) + "]",
                        d,
                        domains_dict['count']
                        ])

                print theader_domain
                dom_choice = -1
                while int(dom_choice) >= len(domains_dict) or int(dom_choice) < 0:
                    dom_choice = raw_input("Select the domain to use for generating email addresses: ")
                    try:
                        dom_choice = int(dom_choice)
                        if dom_choice < len(domains_dict) and dom_choice >= 0:
                            self.domain = domains_dict[dom_choice] 
                            print "[+] Using %s" % (self.domain)
                    except:
                        print "[-] Invalid choice"
                        dom_choice = -1

    def get_requests(self, configDict):
        jig_url = ["connect.data.com"]
        choice = -1
        choices = set([])
        tmp_list = []
        ready = False
        jig_search = gSearch(self.comp_name, configDict, jig_url, self.proxies)
        jig_search.get_company()
        self.glist = jig_search.glist
        print "[+] For multiple URL selections enter one at a time. Scraping will start if you choose to enter your own URL or by pressing <ENTER> without any input. Or search again, I don't care."
        print "[+] NOTE: URLs https://connect.data.com/directory/company/list/ and https://connect.data.com/view/company/ are the same, only need to scrape one if both are found."
        while not ready:
            try:
                choice = int(raw_input("Select a URL to scrape: "))
                if choice > (len(self.glist)+1) or choice < 0:
                    print "[-] Invalid selection. Value has to be between %d and %d" % (0, len(self.glist)+1)
                elif int(choice) == len(self.glist):
                    entered_url = raw_input("Enter a connect.data.com URL: ")
                    self.glist.append({'count': choice, 'company': u'Enter your own URL', 'link': entered_url})
                    choices.add(choice)
                    ready = True
                elif int(choice) == len(self.glist)+1:
                    print "[+] Searching again..."
                    name = raw_input("Enter company name to search: ")
                    jig_search = gSearch(self.comp_name, configDict, jig_url, self.proxies)
                    jig_search.get_company()
                    self.glist = jig_search.glist
                else:
                    choices.add(choice)
            except ValueError:
                if len(choices) == 0:
                    print "[-] No URLs selected yet."
                else:
                    ready = True
        print "%s%d%s" % ("\n[+] Scraping ", len(choices)," URL(s)")
        
        for c in choices:
            targeturl = [ result['link'] for result in self.glist if result['count'] is c ]
            url = targeturl[0]
            self.jig_login(url)
    
    def jig_login(self, url):
        if self.proxy != "":
            service_args = [
                '--load-images=false',
                '--ignore-ssl-errors=true',
                '--proxy='+self.proxy,
                '--proxy-type=http',
                ]
        else:
            service_args = [
                '--load-images=false',
                '--ignore-ssl-errors=true',
                ]

        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap["phantomjs.page.settings.userAgent"] = (
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0"
            )
        browser = webdriver.PhantomJS(service_args=service_args, desired_capabilities=dcap)
        browser.set_window_size(1024, 768)
        browser.get('https://connect.data.com/login')
        if self.j_email != "":
            print "[+] connect.data.com email address found in config.ini"
        else:
            self.j_email = raw_input("[-] No connect.data.com email address set in config.ini, account required for scraping, enter email address: ")

        print "[+] Logging into connect.data.com..."
        username = browser.find_element_by_name('j_username')
        password = browser.find_element_by_name('j_password')

        pdub = getpass.getpass("Enter your data.com password: ")
        username.send_keys(self.j_email)
        password.send_keys(pdub)
        
        form = browser.find_element_by_name('LoginForm')
        form.submit()
        url_split = url.split('/')
        if url.find('list') != -1:
            comp_id = url_split[6] #extract company ID for URL (https://connect.data.com/directory/company/list/)
        else:
            comp_id = url_split[5] #extract company ID for URL (https://connect.data.com/company/view/)

        comp_url = "https://connect.data.com/company/view/" + comp_id
        browser.get(comp_url) #data.com changed the URL to access the employee records
        print "[+] Requesting company info page for " + self.comp_name
        # wait for page to fully load, just in case
        try:
            WebDriverWait(browser, 30).until(EC.visibility_of_element_located((By.ID, "tabs")))
            content = browser.page_source
            soup = BeautifulSoup(content, 'html.parser')
            search_url = soup.select("div.company-counts.company-counts-total-active")[0].a.get("href")
            totalRecords = soup.select("div.company-counts.company-counts-total-active")[0].a.text
            browser.get("https://connect.data.com" + search_url)
            print "[+] Successfully logged in"
            print "[+] " + totalRecords + " records found"
            print "[+] Accessing contacts page..."
            
            #wait for the dynamically loaded elements to show up
            WebDriverWait(browser, 30).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "td.td-name.break-word.name")))
            totalPages = browser.find_element_by_id('totalPages').text

            self.scrape_jig(browser, totalPages)
        except TimeoutException:
            print "[-] Shit, looks like the site timed out, or you entered your password wrong. Try running the script again"
            browser.quit()
            sys.exit(0)

    def scrape_jig(self, browser, totalPages):
        records = [] 
        pbar = ProgressBar()

        for i in pbar(range(0,int(totalPages))):
            js = '''
                var name = document.getElementsByClassName('td-name break-word name');
                return name
                '''
            name_html = browser.execute_script(js)

            js = '''
                var title = document.getElementsByClassName('td-title break-word title');
                return title;
                '''
            title_html = browser.execute_script(js)

            js = '''
                var city = document.getElementsByClassName('td-city city');
                return city;
                '''
            city_html = browser.execute_script(js)

            js = '''
                var state = document.getElementsByClassName('td-state state');
                return state;
                '''
            state_html = browser.execute_script(js)

            for i in range(len(name_html)):
                record_dict = {}
                name = name_html[i].text
                firstname = name[name.find(',')+2:len(name)]
                lastname = name[0:name.find(',')]
                fullname = firstname + " " + lastname
                fname = firstname.lower()
                lname = lastname.lower()
                record_dict["Employee"] = fullname
                record_dict["Role"] = title_html[i].text
                record_dict["City"] = city_html[i].text
                record_dict["State"] = state_html[i].text
                
                if self.emailformat[0] == "<first initial><last name>":
                    record_dict["Email Address"] = fname[0] + lname + self.domain
                elif self.emailformat[0] == "<first initial>.<last name>":
                    record_dict["Email Address"] = fname[0] + '.' + lname + self.domain
                elif self.emailformat[0] == "<first name><last name>":
                    record_dict["Email Address"] = fname + lname + self.domain
                elif self.emailformat[0] == "<first name>.<last name>":
                    record_dict["Email Address"] = fname + '.' + lname + self.domain
                elif self.emailformat[0] == "<first name>":
                    record_dict["Email Address"] = fname + self.domain
                elif self.emailformat[0] == "<last name>":
                    record_dict["Email Address"] = lname + self.domain
                elif self.emailformat[0] == "<first name><last initial>":
                    record_dict["Email Address"] = fname + lname[0] + self.domain
                elif self.emailformat[0] == "<first name>.<last initial>":
                    record_dict["Email Address"] = fname + '.' + lname[0] + self.domain
                elif self.emailformat[0] == "<last name><first initial>":
                    record_dict["Email Address"] = lname + fname[0] + self.domain
                elif self.emailformat[0] == "<last name>.<first initial>":
                    record_dict["Email Address"] = lname + '.' + fname[0] + self.domain
                elif self.emailformat[0] == "<last name><first name>":
                    record_dict["Email Address"] = lname + fname + self.domain
                elif self.emailformat[0] == "<last name>.<first name>":
                    record_dict["Email Address"] = lname + '.' + fname + self.domain
                else:
                    record_dict["Email Address"] = "Not Found"
                record_dict["Generic Phone"] = "Not Found"
                record_dict["Direct Phone"] = "Not Found"
                records.append(record_dict)

            browser.find_element_by_id('next').click()
            time.sleep(2) # wait for next page to fully load, otherwise the script will scrape the same records
        
        browser.quit()
        print "[+] Closing browser..."
        print "[+] Writing " + str(len(records)) + " records to " + self.outfile
        if self.dbflag:
            write_dbfile(records, self.outfile, "connect.data.com") 
        else:
            write_outfile(records, self.outfile, "connect.data.com") 
