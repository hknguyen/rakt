# RAKT - Reconnaissance Assault Killchain Toolkit

## Objective
```
RAKT was created as a means to parse data from business-lists.com, list-brokers.com, connect.data.com
and linkedin.com  as it contains information that can assist in certain information gathering and 
reconnaissance activities. The tool supports both google company name queries and feeding in a list 
of pre-collected  business-list formatted urls. The latter is useful if these were identified through 
previous search engine queries.
```


## Usage
```
$ python rakt.py -h
usage: rakt.py [-h] [-v] {scrape,query} ...

Info: RAKT was created by Hao Nguyen and Chris Patten
Purpose: Scrape contact information from listsite.net, business-lists.com, list-brokers.com, connect.data.com and linkedin.com
Contact: neygun[t.a.]gmail.com; @neygun
Contact: cpatten[t.a.]packetresearch.com; @packetassailant

positional arguments:
  {scrape,query}

optional arguments:
  -h, --help      show this help message and exit
  -v, --version   show program's version number and exit

$ python rakt.py scrape -h
usage: rakt.py scrape [-h] (-o OUTFILE | -d DBOUTFILE) (-s SEARCH | -i INFILE)
                      [-b] [-j] [-l] [-p PROXY]

optional arguments:
  -h, --help            show this help message and exit
  -o OUTFILE, --outfile OUTFILE
                        Output text file
  -d DBOUTFILE, --dboutfile DBOUTFILE
                        Output LevelDB file
  -s SEARCH, --search SEARCH
                        Company to search for in quotes (e.g. 'Company Name')
  -i INFILE, --infile INFILE
                        List of business-lists URLs
  -p PROXY, --proxy PROXY
                        Proxy Host:Port (e.g. 127.0.0.1:8080)

  Select source to scrape, no flags default to all sources

  -b, --blists          Scrape business-lists
  -j, --jigsaw          Scrape connect.data.com
  -l, --linkedin        Scrape linkedin

$ python rakt.py query -h
usage: rakt.py query [-h] [-a] [-n] [-t] [-e] [-p] [-l] -d DBFILE

optional arguments:
  -h, --help            show this help message and exit
  -d DBFILE, --dbfile DBFILE
                        Database to search

  -a, --all             All
  -n, --name            Name
  -t, --title           Title
  -e, --email           Email
  -p, --phone           Phone
  -l, --location        Location
```

## Installation
```
# Installation
# -----------------------------------------------
# rakt was tested on Ubuntu 12.04 and OSX Mavericks
# ----------- OSX ---------------
# OSX Deps: pip install -U -r requirements.txt
# ----------- Linux -------------
# Linux: sudo apt-get install python-pip
# Linux Deps: pip install -U -r requirements.txt
# ----------- Required ----------
# Install LevelDB
# Google: Both an CSE API and CX key
#       CX key - https://www.google.com/cse/
#           Add new custom search engine
#           Add to "Sites to search":
#		www.listsite.net
#               www.business-lists.com
#               www.list-brokers.com
#               connect.data.com
#           Click "Search engine ID" to get the CX key
#       API key - https://console.developers.google.com/project
#           Create Project
#           APIs & auth -> APIs to enable Custom Search
#           APIs & auth -> Credentials -> Create new Key
#           Use Server key
# 
# connect.data.com: Records are now only accessible with a valid account. Sign-up is currently free 
#           at https://connect.data.com/registration/signup
#	    Install PhantomJS.
#
# LinkedIn: Vetted API key and developer token - https://www.linkedin.com/secure/developer
#            Currently only keys and tokens created before 11/2013 when the Vetted API Access 
#            process was introduced will work. Leaving the LinkedIn key and token blank
#            will produce a 403 error
# Add all keys and tokens to config.ini
```

## Sample Run
```
$ python rakt.py scrape -s "fishnet security" -d fns.db -p 127.0.0.1:8080
[-] The web search returned no results for list-brokers.com/search/detail.asp...

[+] 5 results found
+-----+--------------------------------------------------------------------------------+-------------------------------------------------------------------------------------+
|  Id | Company                                                                        | Reference                                                                           |
+-----+--------------------------------------------------------------------------------+-------------------------------------------------------------------------------------+
| [0] | Fishnet Security - Kansas City, MO- Computer related consulting services       | http://www.business-lists.com/search/detail.asp?id=1F75C121C1D819F96C1DF13012451726 |
| [1] | Fishnet Security - Saint Paul, MN- Security Systems Services                   | http://www.business-lists.com/search/detail.asp?id=B63C9201FFD8A78A028569C2E5207A39 |
| [2] | Fishnet Security Inc - Addison, TX- Personal document and information services | http://www.business-lists.com/search/detail.asp?id=AE30B99CFE0570C94A0ADBDC67B8FD4E |
| [3] | Fishnet Security - Dublin, OH- Guard services                                  | http://www.business-lists.com/search/detail.asp?id=068690A8B3A09DF2B7FF13210BC4ACED |
| [4] | Fishnet Security - Papillion, NE- Guard services                               | http://www.business-lists.com/search/detail.asp?id=335C5AA33F1D4A58AF0B4207C7E79DE3 |
| [5] | Enter your own URL                                                             | Enter your own URL                                                                  |
+-----+--------------------------------------------------------------------------------+-------------------------------------------------------------------------------------+
[+] Enter multiple URL selections one at a time. Enter your own URL or press <ENTER> to start scraping.
Select a URL to scrape: 1
Select a URL to scrape: 

[+] Scraping 1 URL(s)

[+] 18 employee record(s) found

[+] The default is 1 request at a time.
Enter the number of simultaneous requests, max of 10. <ENTER> for default: 10
[+] Using the value of 10 request(s)...Get a cup of coffee...This can take a minute
100% (2 of 2) |###############################################################################################################################| Elapsed Time: 0:00:10 Time: 0:00:10
[-] Error printing employee data, will save to file.
[+] 18 result(s) written to fns.db
[-] Unknown Format: Dan Thormodsgaard - thor@fishnetsecurity.com
[-] Unknown Format: Tom St Peter - tom.st.peter@fishnetsecurity.com
[-] Unknown Format: Mary Larson - sms@fishnet.com
[-] Unknown Format: John McGovern - jmac@fishnet.com
[-] Unknown Format: Gerri Migletz - gerri-cb-re@fishnet.com
[-] Unknown Format: Abu-safieh Hytham - hytham.abu-safieh@fishnetsecurity.com
[-] Unknown Format: Dennis Migletz - montreux-usa@fishnet.com

[+] Determining email format...
+------+-------------------------------------------+-------+
| ID   | Email Format                              | Count |
+------+-------------------------------------------+-------+
| [0]  | <first name><last name>                   | 1     |
| [1]  | <first name>.<last name>                  | 9     |
| [2]  | <first initial>.<last name>               | 0     |
| [3]  | <first initial><last name>                | 1     |
| [4]  | <first name>.<last initial>               | 0     |
| [5]  | <first name>                              | 0     |
| [6]  | <first name>.<middle initial>.<last name> | 0     |
| [7]  | <last name><first initial>                | 0     |
| [8]  | <first name><last initial>                | 0     |
| [9]  | <last name>.<first initial>               | 0     |
| [10] | other                                     | 7     |
+------+-------------------------------------------+-------+
Select the format to use for generating email addresses: 
[-] Invalid choice
[+] Highest count for a valid format is 9
[+] Defaulting to choice 1

[+] Discovered domains:
+-----+-----------------------+-------+
| ID  | Domain                | Count |
+-----+-----------------------+-------+
| [0] | @fishnetsecurity.com  | 13    |
| [1] | @fishnet.com          | 4     |
| [2] | @realtyexecutives.com | 1     |
+-----+-----------------------+-------+
Select the domain to use for generating email addresses: 0
[+] Using @fishnetsecurity.com

[+] 2 results found
+-----+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------+
|  Id | Company                                                               | Reference                                                                                      |
+-----+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| [0] | FishNet Security Company Directory of Business Contacts from Data ... | https://connect.data.com/directory/company/list/qPKUuAJ1gS795Kfji7hGfg/fishnet-security        |
| [1] | FishNet Security - Data.com Connect                                   | https://connect.data.com/directory/company/list/qPKUuAJ1gS795Kfji7hGfg/fishnet-security?page=3 |
| [2] | Enter your own URL                                                    | Enter your own URL                                                                             |
+-----+-----------------------------------------------------------------------+------------------------------------------------------------------------------------------------+
[+] Enter multiple URL selections one at a time. Enter your own URL or press <ENTER> to start scraping.
Select a URL to scrape: 0
Select a URL to scrape: 

[+] Scraping 1 URL(s)
[+] Received X-AA-Challenge: 407893 with X-AA-Challenge-ID: 3234056 
[+] Calculated X-AA-Challenge-Result: -122370183
[+] Using X-AA-Cookie-Value: BotMitigationCookie_1969904701684545095="407893001425054059d2yMn0RjrItyLOp3Z4JEsSYRGwM="
100% (3 of 3) |##################################################################################################################################| Elapsed Time: 0:00:00 Time: 0:00:00
[+] 304 result(s) written to fns.db
[+] Scraping LinkedIn...
[+] Only one coId found, using [9291] - FishNet Security
[+] 580 employees found
[-] Discovered employees greater than LinkedIn API throttle of 250
[+] Number of records scraped will be limited due to the throttle
[+] 110 result(s) written to fns.db

$ python rakt.py query -ntep -d fns.db/
Employee;Role;Department;Level;Email Address;Generic Phone;Direct Phone;
Rick Eggleston;Sr. Executive Manager FishnetSecurity Inc.;Not Found;Not Found;rick.eggleston@fishnetsecurity.com;Not Found;Not Found;
Justin George;The Lead Agent;Other;Staff;justin.george@fishnetsecurity.com;Not Found;Not Found;
Christopher Hoff;Security Engineer;IT & IS;Staff;christopher.hoff@fishnetsecurity.com;Not Found;Not Found;
Ray Tam;Director of Strategic Services Sales - Iam Ca and Southwest;Sales;Director-Level;ray.tam@fishnetsecurity.com;Not Found;Not Found;
Mike Bossert;EVP Sales and Marketing at FishNet Security;Not Found;Not Found;mike.bossert@fishnetsecurity.com;Not Found;Not Found;
Larry Scott;Realtor;Sales;Staff;larry.scott@fishnetsecurity.com;Not Found;Not Found;
Jon Wright;Security Consultant;Operations;Staff;jon.wright@fishnetsecurity.com;Not Found;Not Found;
Jason Hicks;Data Security Team Lead;Operations;Staff;jason.hicks@fishnetsecurity.com;Not Found;Not Found;
Jason Grounds;Account Executive;Sales;Staff;jason.grounds@fishnetsecurity.com;Not Found;Not Found;
Dennis Migletz;Realtor;Not Found;Not Found;montreux-usa@fishnet.com;651-225-4478;651-452-5950;
Jill Haning;Accountant;Finance & Administration;Staff;jill.haning@fishnetsecurity.com;Not Found;Not Found;
Paul Rodrigues;Account Executive;Sales;Staff;paul.rodrigues@fishnetsecurity.com;Not Found;Not Found;
<SNIP>
```

## Developing
```
Alpha code under active development (version 0.1)
```

## Contact
```
# Author: Hao Nguyen
# Contact (Email): neygun[t.a.]gmail[t.o.d]com
# Contact (Twitter): neygun
#
# Author: Chris Patten
# Contact (Email): cpatten[t.a.]packetresearch[t.o.d]com
# Contact (Twitter): packetassailant
```
